package pato;
//Ejercicio de un cajero que tiene la opcion de consulta "opcion de deposito", "opcion de retiro".
//mostrarle al usuario por pantalla que opcion ha seleccionado
import java.util.Scanner;
public class Ej3OpcionDeCajero {
	public static void main(String[] args) {
	Scanner EN = new Scanner(System.in);
	
		System.out.println("Seleccione una opcion: ");
		System.out.print("1) Consulta ");
		System.out.print("2) Deposito ");
		System.out.println("3) Retiro");
		
		int select = EN.nextInt();
		
		switch(select) {
		case 1:
			System.out.print("Ha seleccionado Consulta");
			break;
		case 2:
			System.out.print("Ha seleccionado Deposito");
			break;
		case 3:
			System.out.print("Ha seleccionado Retiro");
			break;
		default:
			System.out.println("Error");
			break;
		}
	}
}
