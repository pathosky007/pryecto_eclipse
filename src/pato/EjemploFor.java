package pato;

import javax.swing.JOptionPane;

public class EjemploFor {
public static void main(String[] args) {
	
	String[] nombres = new String[3];
	nombres[0] = "carla";
	nombres[1] = "patricia";
	nombres[2] = "andres";
	
	for(int i=0; i<3; i++) {
		JOptionPane.showMessageDialog(null,nombres[i]);
		System.out.println(nombres[i]);
	}
}
}
