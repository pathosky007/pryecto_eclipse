//mostrar numeros de 0 a 100 de 7 en 7
package pato;

public class EjercicioDoWhile {
	public static void main(String[] args) {
		
		int contador=0;
		
		do 
		{	
			System.out.println(":"+contador);
			contador=contador+7;
		}
		while (contador <= 100);
	}

}
